<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/bulma.css">
    <title>Exemple PHP</title>
</head>
<body>
    <header>
        <div class="container is-fluid">
            <form action="/" method="GET" >
                <input class="input is-medium" type="text" name="prenom"  placeholder="Recherche"/><br />
                <input type="submit" value="Soumettre"/>
            </form>
        </div>
    </header>

<hr>
<section class="hero">
  <div class="hero-body">
    <div class="container is-fluid">
        <form action="/" method="POST" >
            <input class="input is-medium" type="text" name="prenom"  placeholder="Prénom"/><br />
            <input class="input is-medium" type="text" name="nom" placeholder="Nom" /><br />
            <textarea class="input" name="comment" id="comment" placeholder="Entrez votre commentaire ici!" cols="30" rows="10"></textarea>
            <br/>
            <div class="control">
                <input type="submit" Value="Commenter" class="button is-primary">
            </div>
        </form>
    </div>
  </div>
</section>
<hr>

<section class="hero">
  <div class="hero-body">
    <div class="container is-fluid">
        <h1 class="title">Commentaires</h1>
    </div>
  </div>
 
</section>
<section>
    <div class="container is-fluid">
        <h5 class="subtitle is-5">Nom complet: Rodrigo Martinez</h5>
        <h6 class="subtitle is-6">Commentaire:</h6>
        <section>
            Enim mollitia quas non. Deleniti quos similique et et et fugit. Sint voluptas qui incidunt.
            Sint eum et placeat quia provident explicabo velit. Id aut voluptas iure dolores sed. Incidunt aut non nemo officia cum mollitia iure. Nihil quos quaerat sapiente enim quas sunt eos. Eum soluta perferendis et recusandae omnis.
            Quia est iusto aut quasi impedit. Omnis et consequuntur excepturi vel. Velit illum corporis molestiae minima qui sit. Aut nobis quia quod libero nesciunt est dolore in. Doloribus quis voluptate quod. Tempore veniam sequi sit enim.
        </section>
    </div>
</section>

</body>
</html>